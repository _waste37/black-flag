#include <str.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

int 
main (void)
{
    str s1, s2;

    str_init(&s1);
    str_push(&s1,'c' );
    str_push(&s1,'i' );
    str_push(&s1,'a' );
    str_push(&s1,'o' );

    str_init_from(&s2, "ciao", 4);
    assert(str_equal(&s1, &s2));
    str_free(&s2);

    str_clear(&s1);
    str_append_cstr(&s1, "ciao come vaa", 13);
    s2 = str_split(&s1,' ');
    assert(s1.len == 4 && 0 == memcmp(s1.raw, "ciao", s1.len));
    assert(s2.len == 8 &&0 == memcmp(s2.raw, "come vaa", s2.len));

    str_push(&s1, ' ');
    str_append(&s1, &s2);
    assert(s1.len == 13 && 0 == memcmp(s1.raw, "ciao come vaa", s1.len));

    str_free(&s1);
    str_free(&s2);

    return 0;
}
