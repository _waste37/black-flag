#include <bigint.h>
#include <assert.h>
#include <stdio.h>


int 
main (void)
{
    bigint i1, i2;
    bigi_from_int(&i1, 10);
    bigi_from_int(&i2, 20);
    assert(bigi_print(&i1, 2) == true);
    bigi_add(&i1, &i1, &i2);
    assert(i1.data[0] == 30);

    bigi_addi(&i1, &i1, 9223372036854775807);
    assert(bigi_gt(&i1, &i2));
    assert(bigi_ge(&i1, &i2));

    bigi_addi(&i2, &i2, 9223372036854775807);
    assert(bigi_le(&i2, &i1));
    bigi_addi(&i2, &i2, 10);
    assert(bigi_eq(&i1, &i2));
    
    assert(bigi_print(&i1, 2) == true);

    bigi_add(&i1, &i1, &i2);
    assert(bigi_print(&i1, 2) == true);
    printf("\n");

    return 0;
}
