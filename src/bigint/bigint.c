#include <bigint.h>
#include <str.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define unimplemented (assert(0==1))

static void *(*bigint_alloc)(void*, size_t) = realloc;
static void (*bigint_free)(void*) = free;


static void 
resz_if_needed(bigint *p, uint64_t needed)
{
    if (p->cap >= needed + 1) {
        return;
    }

    while (p->cap < needed + 1) {
        p->cap <<= 1;
    }
    p->data = bigint_alloc(p->data, p->cap * sizeof(uint64_t));
}

void 
bigi_init(bigint *i)
{
    i->cap = 1;
    i->len = 1;
    i->data = bigint_alloc(i->data, i->cap * sizeof(uint64_t));
    i->data[0] = 0;
}

void 
bigi_from_int(bigint *i, int64_t val)
{
    bigi_init(i);
    bigi_addi(i, i, val);
}

bool
bigi_from_str();

void 
bigi_free(bigint *i)
{
    i->cap = 0;
    i->len = 0;
    bigint_free(i->data);
    i->data = NULL;
}
#include <assert.h>
static str
bigi_to_str_bin(const bigint *i)
{
    str s;
    uint32_t find = i->data[i->len - 1], digit_count = 0;
    while(find) {
        digit_count++;
        find >>=1;
    }

    int32_t t = ((i->len - 1) * 32) + digit_count;
    str_init(&s);
    str_resize(&s, 1 + t--);

    for (uint64_t j = 0; j < i->len; ++j) {
        uint32_t n = i->data[j];
        uint8_t bound = j == i->len - 1 ? digit_count : 32;
        for (uint8_t k = 0; k < bound; ++k) {
            if (n & 1) {
                str_insert(&s, '1', t--);
            } else {
                str_insert(&s, '0', t--);
            }
            n >>= 1;
        }
    }
    return s;
}

str
bigi_to_str(const bigint *i, int8_t radix)
{
    switch(radix) {
        case 2:
            return bigi_to_str_bin(i);
        default:
            return (str){0};
    }
}

bool 
bigi_print(const bigint *i, int8_t radix)
{
    str s = bigi_to_str(i, radix);
    if (s.raw == 0) {
        return false;
    }

    printf("%.*s", s.len, s.raw);
    str_free(&s);
    return true;
}


void 
bigi_add(bigint *dst, const bigint *i1, const bigint *i2)
{
    const bigint *n1, *n2;
    n1 = i1->len < i2->len ? i1 : i2;
    n2 = i1->len < i2->len ? i2 : i1;
    uint32_t carry = 0;
    for (size_t i = 0; i < n1->len; ++i) {
        uint64_t res = (uint64_t)n1->data[i] + (uint64_t)n2->data[i] + (uint64_t)carry;
        carry = res >> 32;
        resz_if_needed(dst, i);
        dst->data[i] = res & 0xffffffff;
    }

    for (size_t i = n1->len; i < n2->len; ++i) {
        uint64_t res = (uint64_t)n2->data[i] + (uint64_t)carry;
        carry = res >> 32;
        resz_if_needed(dst, i);
        dst->data[i] = res & 0xffffffff;
    }

    dst->len = n2->len;
    if (carry != 0) { 
        resz_if_needed(dst, dst->len);
        dst->data[dst->len] = carry;
        dst->len++;
    }
}

void 
bigi_sub(bigint *dst, const bigint *i1, const bigint *i2)
{
    unimplemented;
}

void 
bigi_mul(bigint *dst, const bigint *i1, const bigint *i2)
{
    unimplemented;
}

void 
bigi_div(bigint *dst, const bigint *i1, const bigint *i2)
{
    unimplemented;
}

void 
bigi_addi(bigint *dst, const bigint *i1, int64_t i2)
{
    uint32_t carry = 0;
    uint64_t res = (uint64_t)i1->data[0] + (uint64_t)i2;
    carry = res >> 32;
    dst->data[0] = res & 0xffffffff;

    if (carry != 0 || dst != i1) {
        for (size_t i = 1; i < i1->len; ++i) {
            uint64_t res = (uint64_t)i1->data[i] + (uint64_t)carry;
            carry = res >> 32;
            resz_if_needed(dst, i);
            dst->data[i] = res & 0xffffffff;
        }
    }
    dst->len = i1->len;
    if (carry != 0) { 
        resz_if_needed(dst, dst->len);
        dst->data[dst->len] = carry;
        dst->len++;
    }
}

void 
bigi_subi(bigint *dst, const bigint *i1, int64_t i2)
{
    unimplemented;
}

void 
bigi_muli(bigint *dst, const bigint *i1, int64_t i2)
{
    unimplemented;
}

void 
bigi_divi(bigint *dst, const bigint *i1, int64_t i2)
{
    unimplemented;
}


bool 
bigi_lt(const bigint *i1, const bigint *i2)
{
    if (i1->len > i2->len) {
        return false;
    } else if (i1->len < i2->len) {
        return true;
    }

    for (uint64_t i = i1->len - 1; i >= 0; --i) {
        if (i1->data[i] < i2->len) {
            return true;
        } else if (i1->data[i] > i2->data[i]) {
            return false;
        }
    }
    return false;
}

bool 
bigi_le(const bigint *i1, const bigint *i2)
{
    if (i1->len > i2->len) {
        return false;
    } else if (i1->len < i2->len) {
        return true;
    }

    for (uint64_t i = i1->len - 1; i >= 0; --i) {
        if (i1->data[i] < i2->len) {
            return true;
        } else if (i1->data[i] > i2->data[i]) {
            return false;
        }
    }
    return true;
}

bool 
bigi_gt(const bigint *i1, const bigint *i2)
{
    return !bigi_le(i1, i2);
}

bool 
bigi_ge(const bigint *i1, const bigint *i2)
{
    return !bigi_lt(i1, i2);
}

bool 
bigi_eq(const bigint *i1, const bigint *i2)
{
    if (i1->len != i2->len) {
        return false;
    }
    for (uint64_t i = 0; i < i1->len; ++i) {
        if (i1->data[i] != i2->data[i]) {
            return false;
        }
    }
    return true;
}

bool 
bigi_ne(const bigint *i1, const bigint *i2)
{
    return !bigi_eq(i1, i2);
}


bool 
bigi_lti(const bigint *i1, int64_t i2)
{
    unimplemented;
}

bool 
bigi_lei(const bigint *i1, int64_t i2)
{
    unimplemented;
}

bool 
bigi_gti(const bigint *i1, int64_t i2)
{
    unimplemented;
}

bool 
bigi_gei(const bigint *i1, int64_t i2)
{
    unimplemented;
}

bool 
bigi_eqi(const bigint *i1, int64_t i2)
{
    unimplemented;
}

bool 
bigi_nei(const bigint *i1, int64_t i2)
{
    unimplemented;
}

