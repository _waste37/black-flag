#include <str.h>
#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define STRLIB_HALVING_THRESHOLD 65536

static void *(*strlib_alloc)(void*,size_t) = realloc;
static void (*strlib_free)(void*) = free;

static void inline
str_resz_if_needed(str *s, size_t needed)
{
    if (needed == 0) {
        strlib_free(s->raw);
        s->raw = NULL;
        return;
    }

    if (s->cap > needed && s->cap < STRLIB_HALVING_THRESHOLD) {
        return;
    }
    
    if(s->cap > needed && s->cap >= STRLIB_HALVING_THRESHOLD) {
        while (s->cap > needed * 2) {
            s->cap >>= 1;
        }
    }
    
    while (s->cap < needed + 1) {
        s->cap <<= 1;
    }
    s->raw = strlib_alloc(s->raw, s->cap * sizeof(char)); 
}


void 
str_init(str *s)
{
    s->cap = 1;
    s->len = 0;
    s->raw = NULL;
    str_resz_if_needed(s, 8);
}

void
str_init_from(str *s, const char *cstr, size_t len) 
{
    s->cap = 1;
    s->len = 0;
    s->raw = NULL;
    str_append_cstr(s, cstr, len);
}

void
str_free(str *s)
{
    s->len = 0;
    s->cap = 0;
    str_resz_if_needed(s, 0);
}

void
str_clear(str *s)
{
    s->len = 0;
}

void 
str_resize(str *s, size_t new_len)
{
    if(s->len < new_len) {
        str_resz_if_needed(s, new_len);
    }
    s->len = new_len;
}

void 
str_fill(str *s, char c)
{
    memset(s->raw, c, s->len);
}

void 
str_append(str *s1, const str *s2)
{
    str_resz_if_needed(s1, s1->len + s2->len);
    memmove(s1->raw + s1->len, s2->raw, s2->len+1);
    s1->len += s2->len;
}

void
str_push(str *s, char c)
{
    str_resz_if_needed(s, s->len + 1);
    s->raw[s->len] = c;
    
    s->len++;
}

char 
str_pop(str *s)
{
    if (s->len == 0) {
        return '\0';
    }
    return s->raw[--s->len];
}

void 
str_insert(str *s, char c, size_t index)
{
    if (index >= s->len) {
        return;
    }

    s->raw[index] = c;
}

void 
str_push_front(str *s, char c)
{
    str_resz_if_needed(s, s->len + 1);
    memmove(s->raw + 1, s->raw, s->len); 

    s->raw[0] = c;
    s->len++;
}

void 
str_append_cstr(str *s, const char *cstr, size_t len)
{
    if (cstr == NULL) {
        return;
    }

    str_resz_if_needed(s, s->len + len);
    memcpy(s->raw + s->len, cstr, len);
    s->len += len;
}

static void
str_zero_terminate(str *s)
{
    if (s->raw[s->len-1] == '\0') {
        return;
    }

    str_push(s, '\0');
}

const char *
str_to_cstr(str *s)
{
    str_zero_terminate(s);
    return s->raw;
}


int32_t
str_compare(const str *s1, const str *s2)
{
    if (s1->len != s2->len ) {
        return *s1->raw - *s2->raw;
    }
    
    for (size_t i = 0; i < s1->len; ++i) {
        if (s1->raw[i] != s2->raw[i]) {
            return s1->raw[i] - s2->raw[i];
        }
    }
    return 0;
}

bool
str_equal(const str *s1, const str *s2)
{
    if (s1->len != s2->len ) {
        return false;
    }
    
    for (size_t i = 0; i < s1->len; ++i) {
        if (s1->raw[i] != s2->raw[i]) {
            return false;
        }
    }
    return true;
}

void 
str_copy (str *dst, const str *src)
{
    str_resz_if_needed(dst, src->len);
    memcpy(dst->raw, src->raw, src->len);
    dst->len = src->len;
}

int32_t
str_find(str *s, char c) 
{
    for (size_t i = 0; i < s->len; ++i) {
        if (s->raw[i] == c) {
            return i;
        }
    }
    return -1;
}

str
str_split_at(str *s, int32_t index)
{
    str ss = {0};
    if (index == 0) {
        return ss;
    } else if (index >= (ssize_t)s->len) {
        return *s;
    }

    str_init(&ss);
    str_resize(&ss, index == -1 ? s->len : (size_t)index);

    memcpy(ss.raw, s->raw, ss.len);

    if (index == -1) {
        str_free(s);
    } else {
        memmove(s->raw, s->raw + index + 1, s->len - index - 1);
        str_resize(s, s->len - index - 1);
    }

    return ss;
}

str
str_split(str *s, char delim)
{
    str ss = str_split_at(s, str_find(s, delim)); 
    return ss;
}

str
str_substring(str *s, size_t start, size_t end)
{
    if (end >= s->len || start >= end) {
        return (str){0};
    }
    size_t l = s->len;

    str ss;
    str_init(&ss);

    str_resize(s, end + 1);
    str_copy(&ss, s);
    str_resize(s, l);
    return ss;
}

void
str_trim(str *s)
{
    size_t i = s->len;
    while (i > 0 && isspace(s->raw[i - 1])) {
        i = i - 1;
    }

    str_resize(s, i+1);
    i = 0;
    while (i < s->len && isspace(s->raw[i++])) /* no-op */;

    s->len = s->len - i;
    memmove(s->raw, s->raw + (isspace(s->raw[i-1]) ? i : 0), s->len);
 
    str_resz_if_needed(s, s->len);
}

void 
str_getline(str *s, FILE *stream)
{
    if (s->raw != NULL) {
        str_free(s);
    }

    if ((ssize_t)(s->len = getline(&s->raw, &s->cap, stream)) == -1) {
        str_free(s);
    }
}

/* strvec ***********************************************************/

static void 
vec_resz_if_needed(strvec *v, size_t needed)
{
    if (needed == 0) {
        strlib_free(v->ss);
        strlib_free(v->raw);
        v->raw = NULL;
        v->ss = NULL;
        return;
    }

    if (v->cap > needed && v->cap < STRLIB_HALVING_THRESHOLD) {
        return;
    }
    
    if(v->cap > needed && v->cap >= STRLIB_HALVING_THRESHOLD) {
        while (v->cap > needed * 2) {
            v->cap >>= 1;
        }
    }
    
    while (v->cap < needed + 1) {
        v->cap <<= 1;
    }

    v->ss = strlib_alloc(v->ss, v->cap * sizeof(str)); 
    v->raw = strlib_alloc(v->raw, v->cap * sizeof(char*)); 
}

void 
strvec_init(strvec *v)
{
    v->cap = 1;
    v->len = 0;
    v->ss = NULL;
    v->raw = NULL;
    vec_resz_if_needed(v, 8);
}
void 
strvec_free(strvec *v)
{
    v->cap = 0;
    v->len = 0;
    vec_resz_if_needed(v, 0);
}

void 
strvec_push(strvec *v, str *s)
{
    vec_resz_if_needed(v, v->len + 1);
    memcpy(v->ss + v->len, s, sizeof(str));
    v->raw[v->len] = s->raw;
    v->len = v->len + 1;
}

void 
strvec_push_cstr(strvec *v, const char *s, size_t len)
{
    vec_resz_if_needed(v, v->len + 1);
    str_init_from(v->ss + v->len, s, len);

    v->raw[v->len] = s;
    v->len = v->len + 1;
}

void 
strvec_push_front(strvec *v, str *s)
{
}

void 
strvec_push_front_cstr(strvec *v, const char *s, size_t len)
{
}

str 
strvec_pop(strvec *v)
{
}

void 
strvec_resize(strvec *v, size_t new_size)
{
}

void 
strvec_clear(strvec *v)
{
}

/********************************************************************/


void
strlib_set_deallocator(void (deallocator)(void*))
{
    strlib_free = deallocator;
}

void
strlib_set_allocator(void *(allocator)(void*, size_t))
{
    strlib_alloc = allocator;
}
