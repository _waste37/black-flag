#ifndef BLACK_FLAG_STR_H
#define BLACK_FLAG_STR_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct
{
    size_t cap;
    size_t len;
    char *raw;
} str;

void str_init(str *s);
void str_init_from(str *s, const char *str, size_t len);

/**
 * @brief free the resources associated with the str.
 * if you want to use it after this, you must call str_init* function.
 */

void str_free(str *s);

/**
 * @brief make the string empty.
 */

void str_clear(str *s);


/**
 * @brief make the string of new_len characters
 * if the new len is greater than the old, the contents of 
 * the new part are undefined
 */

void str_resize(str *s, size_t new_len);

void str_fill(str *s, char c);

/**
 * @brief Append str to str
 */

void str_append(str *s1, const str *s2);

/**
 * @brief append single char to the end of the str
 */

void str_push(str *s, char c);

char str_pop(str *s);
/**
 * @brief insert character at index
 */

void str_insert(str *s, char c, size_t index);

/**
 * #brief insert character at the beginning of the str
 */

void str_push_front(str *s, char c);

/*
 * @brief removes starting and ending whitespaces in str
 */

void str_trim(str *s);

/** 
 * @brief append cstring to str
 */

void str_append_cstr(str *s, const char *cstr, size_t len);

/**
 *
 * you must copy the data returned if you want to own it.
 * DONT'T take references to the data returned by this function
 * it is owned by the str in input.
 */
const char *str_to_cstr(str *s);

/**
 * @brief behaves like the standard strcmp
 */

int32_t str_compare(const str *s1, const str *s2);

/**
 * @brief returns true if the strings are equal.
 */
bool str_equal(const str *s1, const str *s2);

/**
 * @brief copy src data into dst. 
 * all data contained in dst will be lost.
 */

void str_copy(str *dst, const str *src);

/**
 * @brief returns the index of the first occurence of c
 *        -1 on failure
 */

int32_t str_find(str *s, char c);

/**
 * @brief resizes the string in input to the char up to the index
 *        and returns a str containing the rest
 * in case of index out of bound the string returned have data set NULL
 */

str str_split_at(str *s, int32_t index);

/**
 * @brief resizes the str in input up to the character before c
 * and returns a str containing the data after c
 */

str str_split(str *s, char c);

/**
 * @brief returns a str containing the data 
 *        starting at start and ending at end.
 *
 * in case of wrong indexes the string returned will have data set NULL
 */

str str_substr(str *s, size_t start, size_t end);

void str_getline(str *s, FILE *stream);

typedef struct 
{
    size_t cap;
    size_t len;
    str   *ss;
    const char **raw;
} strvec;

void strvec_init(strvec *v);
void strvec_free(strvec *v);

void strvec_push(strvec *v, str *s);
void strvec_push_cstr(strvec *v, const char *s, size_t len);
void strvec_push_front(strvec *v, str *s);
void strvec_push_front_cstr(strvec *v, const char *s, size_t len);

str strvec_pop(strvec *v);
void strvec_resize(strvec *v, size_t new_size);
void strvec_clear(strvec *v);

void strlib_set_allocator(void *(allocator)(void *, size_t));
void strlib_set_deallocator(void (deallocator)(void *));

#endif /* BLACK_FLAG_STR_H */
