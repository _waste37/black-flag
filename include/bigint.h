#ifndef BLACK_FLAG_BIGINT_H
#define BLACK_FLAG_BIGINT_H

#include <stdint.h>
#include <stdbool.h>

typedef struct 
{
    uint64_t cap;
    uint64_t len;
    uint32_t *data; 
} bigint;

void bigi_init(bigint *i);
void bigi_from_int(bigint *i, int64_t val);
void bigi_free(bigint *i);

/* input output */

char *bigi_to_string(const bigint *i, int8_t radix);
bool bigi_from_string(bigint *i, int8_t radix);
bool bigi_print(const bigint *i, int8_t radix);

/* operations */

void bigi_add(bigint *dst, const bigint *i1, const bigint *i2);
void bigi_sub(bigint *dst, const bigint *i1, const bigint *i2);
void bigi_mul(bigint *dst, const bigint *i1, const bigint *i2);
void bigi_div(bigint *dst, const bigint *i1, const bigint *i2);
void bigi_mod(bigint *dst, const bigint *i1, const bigint *i2);

void bigi_addi(bigint *dst, const bigint *i1, int64_t i2);
void bigi_subi(bigint *dst, const bigint *i1, int64_t i2);
void bigi_muli(bigint *dst, const bigint *i1, int64_t i2);
void bigi_divi(bigint *dst, const bigint *i1, int64_t i2);
void bigi_modi(bigint *dst, const bigint *i1, int64_t i2);

/* comparing */
bool bigi_lt(const bigint *i1, const bigint *i2);
bool bigi_le(const bigint *i1, const bigint *i2);
bool bigi_gt(const bigint *i1, const bigint *i2);
bool bigi_ge(const bigint *i1, const bigint *i2);
bool bigi_eq(const bigint *i1, const bigint *i2);
bool bigi_ne(const bigint *i1, const bigint *i2);

bool bigi_lti(const bigint *i1, int64_t i2);
bool bigi_lei(const bigint *i1, int64_t i2);
bool bigi_gti(const bigint *i1, int64_t i2);
bool bigi_gei(const bigint *i1, int64_t i2);
bool bigi_eqi(const bigint *i1, int64_t i2);
bool bigi_nei(const bigint *i1, int64_t i2);

#endif /* BLACK_FLAG_BIGINT_H */
